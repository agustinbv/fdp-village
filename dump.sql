CREATE TABLE `interno_entradas` (
      `ID` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
      `codigo` varchar(20) DEFAULT NULL,
      `tipo` enum('3d','comun','monster') NOT NULL,
      `precio` int(11) NOT NULL,
      `user` varchar(16) DEFAULT NULL,
      `vencimiento` date NOT NULL,
      `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated Date',
      `created` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Created Time',
      PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8
