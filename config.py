import os


MYSQL_USER = os.environ.get('MYSQL_USER', 'interno')
MYSQL_PASS = os.environ.get('MYSQL_PASS', '')
MYSQL_HOST = os.environ.get('MYSQL_HOST', 'localhost')
MYSQL_DB = os.environ.get('MYSQL_DB', 'interno')


class Config(object):
    UPLOAD_FOLDER = os.path.join(os.path.dirname(__file__), 'uploads')
    MAX_CONTENT_LENGTH = 16 * 1024 * 1024
    SECRET_KEY = "A0Zr98fkajsdlkfH!jmN]LWX/,?RT"
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    BASIC_AUTH_USERNAME = os.environ.get('BASIC_AUTH_USERNAME')
    BASIC_AUTH_PASSWORD = os.environ.get('BASIC_AUTH_PASSWORD')


class ProductionConfig(Config):
    DEBUG = False
    SQLALCHEMY_DATABASE_URI = "mysql+pymysql://{}:{}@{}/{}".format(
        MYSQL_USER, MYSQL_PASS, MYSQL_HOST, MYSQL_DB
    )


class DevelopmentConfig(Config):
    DEBUG = True
    BASIC_AUTH_USERNAME = 'admin'
    BASIC_AUTH_PASSWORD = 'admin'
    SQLALCHEMY_DATABASE_URI = "mysql+pymysql://root:dukenukem@localhost/interno"
    
