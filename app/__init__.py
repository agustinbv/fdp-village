import os
import logging
import datetime 

from flask import Flask, request, flash, redirect, url_for, render_template
from flask.ext.basicauth import BasicAuth
from flask_sqlalchemy import SQLAlchemy
from werkzeug import secure_filename

app = Flask(__name__)

app.config.from_object('config.DevelopmentConfig')

if os.environ.get('PRODUCTION'):
    app.config.from_object('config.ProductionConfig')
    logger = logging.StreamHandler()
    logger.setLevel(logging.INFO)
    app.logger.addHandler(logger)

# database
db = SQLAlchemy(app)
db.Model.metadata.reflect(db.engine)

# basic auth
basic_auth = BasicAuth(app)

from app import village, models


@app.route('/')
@basic_auth.required
def index():
    hoy = datetime.date.today()

    precios = {}    
    
    for tipo in ("comun", "3d", "monster"):
        precios[tipo] = models.Cupon.query.filter(
            models.Cupon.tipo == tipo
        ).order_by(-models.Cupon.updated).value(
            models.Cupon.precio
        )

    return render_template('index.html', hoy=hoy, precios=precios)


@app.route('/preview', methods=['POST'])
@basic_auth.required
def preview():
    doc = request.files['doc']
    
    if doc.filename.lower().endswith('.pdf'):

        path = os.path.join(
            app.config['UPLOAD_FOLDER'], 
            secure_filename(doc.filename)
        )

        # guardo el pdf
        doc.save(path)

        # parse
        v = village.VillageFDP(path, data=request.form)
        codigos = v.parse_codigos()

        ctx = { 
            "path": path,
            "codigos": codigos, 
            "form": request.form 
        }

        return render_template('preview.html', **ctx)
    else:
        flash("el archivo no es pdf", "danger")

    return redirect(url_for('index'))


@app.route('/upload', methods=['POST'])
@basic_auth.required
def upload():
    app.logger.info('uploading!...')

    path = request.form.get('path')

    # import
    v = village.VillageFDP(path, data=request.form)
    success, response = v.import_codigos()
    
    if success:
        flash("subido.", "info")
    else:
        flash("ERROR: {}".format(response), "danger")

    # borro el pdf
    os.remove(path)

    return redirect(url_for('index'))