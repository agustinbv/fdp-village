import traceback
import logging
import subprocess

from app import db
from models import Cupon


class VillageFDP(object):

    def __init__(self, pdf_path, data={}):
        self.pdf_path = pdf_path
        self.data = data

    def parse_codigos(self):
        text = subprocess.check_output(["pdf2txt.py", self.pdf_path])
        start = self.data.get("__s", "V10")
        codigos = filter(lambda i: i.startswith(start), text.split())
        return codigos

    def import_codigos(self, codigos=None):
        if not codigos:
            codigos = self.parse_codigos()

        try:
            for codigo in codigos:

                item = {
                    "codigo": codigo,
                    "tipo": self.data.get('tipo'),
                    "precio": self.data.get('precio'),
                    "vencimiento": self.data.get('vencimiento')
                }

                cupon = Cupon(**item)
                db.session.add(cupon)

            db.session.commit()

        except Exception as e:
            return False, traceback.format_exc()

        return True, "success"
