from app import db


class Cupon(db.Model):
    __table__ = db.Model.metadata.tables['interno_entradas']

    def __init__(self, *args, **kwargs):
        self.codigo = kwargs.get('codigo')
        self.tipo = kwargs.get('tipo')
        self.vencimiento = kwargs.get('vencimiento')
        self.precio = kwargs.get('precio')
        self.updated = kwargs.get('updated')